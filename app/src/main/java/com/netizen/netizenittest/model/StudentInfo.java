package com.netizen.netizenittest.model;

public class StudentInfo {

    private String SName;
    private String SImage;
    private String SSession;
    private String SClass;
    private String SEmail;
    private String SBirthDate;
    private String SBloodGroup;
    private String SContactPersion;
    private String SArea;
    private String SPhone;
    private String SCity;
    private String SPinCode;
    private String SGender;
    private String SAgree;


    public StudentInfo(String SName, String SImage, String SEmail, String SBirthDate, String SBloodGroup,
                       String SContactPersion, String SArea, String SPhone, String SCity, String SPinCode,
                       String SGender, String SAgree) {
        this.SName = SName;
        this.SImage = SImage;
        this.SEmail = SEmail;
        this.SBirthDate = SBirthDate;
        this.SBloodGroup = SBloodGroup;
        this.SContactPersion = SContactPersion;
        this.SArea = SArea;
        this.SPhone = SPhone;
        this.SCity = SCity;
        this.SPinCode = SPinCode;
        this.SGender = SGender;
        this.SAgree = SAgree;
    }

    public String getSName() {
        return SName;
    }

    public void setSName(String SName) {
        this.SName = SName;
    }

    public String getSImage() {
        return SImage;
    }

    public void setSImage(String SImage) {
        this.SImage = SImage;
    }

}
